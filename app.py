import os
import random

import numpy as np
import plotly.graph_objects as go
from dash import Dash, Input, Output, dcc, html

from spline import get_natural_cubic_spline_model


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server


dim = 300
sent = 'Attention is all you need'

black_count = 5

sent_array = np.load('sent_array.npy')
full_array = np.load('full_array.npy')

red = 'rgba(188, 0, 45, 1)'
black = 'rgba(0, 0, 0, 1)'

knots = 10

width = 0.015
freq1 = 0.10
freq2 = 0.02
ampl = 0.003

red_mult = 1.5

op = 1

otter = ''.join('{:08b}'.format(b) for b in '🦦'.encode('utf8'))
word_bit = ['{:08b}'.format(b) for b in 'AIAUN'.encode('utf8')]


def bit_plot(bits, y):
    y_dot = []
    x_dot = []
    for idx, bit in enumerate(bits):
        if bit == '1':
            num = int(idx * 300 / len(bits))
            y_dot.append(y[num])
            x_dot.append(num + 1)
    return np.array(x_dot), np.array(y_dot)


def var_width(fig, color, x, y_est, width, freq1, freq2, ampl):
    y_est1 = y_est
    y_est2 = y_est
    for i in range(10):
        f1 = random.random()*abs(freq1-freq2) + max(freq1, freq2)
        f2 = random.random()*abs(freq1-freq2) + max(freq1, freq2)
        phi1 = random.random()*(6.28)
        phi2 = random.random()*(6.28)
        y_est1 = y_est1 + random.random() * ampl * np.sin(x*f1 + phi1)
        y_est2 = y_est2 - random.random() * ampl * np.sin(x*f2 + phi2)
    y_est1 += width
    fig.add_trace(go.Scatter(x=np.concatenate([x,x[::-1]]), y=np.concatenate([y_est2, y_est1[::-1]]), mode='lines', name=None, line_color=color, fill='toself', fillcolor=color))


def gen_lines(full_array, sent_array):
    x = np.linspace(1, dim, dim)
    lines = full_array[np.random.choice(len(full_array), size=black_count, replace=False)]

    black_plot = []
    for i, bits in zip(lines, word_bit):
        model = get_natural_cubic_spline_model(x, i, minval=min(x), maxval=max(x), n_knots=knots)
        y_est = model.predict(x)
        x_dot, y_dot = bit_plot(bits, y_est)
        black_plot.append((y_est, x_dot, y_dot))


    model = get_natural_cubic_spline_model(x, sent_array.mean(axis=0), minval=min(x), maxval=max(x), n_knots=knots)
    y_est = model.predict(x)
    x_dot, y_dot = bit_plot(otter, y_est)
    red_plot = (y_est, x_dot, y_dot)
    return black_plot, red_plot, x


def gen_figure(black_plot, red_plot, x):
    fig = go.Figure()

    for y_est, x_dot, y_dot in black_plot:
        #clr = random.randint(25, 25)
        clr = 25
        color = f'rgba({clr}, {clr}, {clr}, {op})'
        var_width(fig, color, x, y_est, width/3, freq1, freq2, ampl/2)
        #fig.add_trace(go.Scatter(x=x_dot, y=y_dot, mode='markers', line_color=red, marker=dict(size=4, symbol=0)))
        #fig.add_trace(go.Scatter(x=x, y=y_est, mode='lines', line_color=f'rgba({clr}, {clr}, {clr}, {op})'))

    y_est, x_dot, y_dot = red_plot
    #fig.add_trace(go.Scatter(x=x_dot, y=y_dot, mode='markers', line_color=black, marker=dict(size=4, symbol=0)))
    #fig.add_trace(go.Scatter(x=x, y=y_est, mode='lines', name=None, line_color=red))
    var_width(fig, red, x, y_est*red_mult, width*red_mult, freq1, freq2, ampl*red_mult)
    return fig

def layout():

    fig = gen_figure(*gen_lines(full_array, sent_array))

    fig.update_layout(
        showlegend=False,
        autosize=False,
        #width=2000,
        #height=500,
        margin=dict(l=0, r=0, b=0, t=0, pad=4),
        xaxis=dict(showgrid=False, zeroline=False, visible=False),
        yaxis=dict(showgrid=False, zeroline=False, visible=False, scaleanchor='x', scaleratio=200),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')

    config = {'staticPlot': True}
    return html.Div(children=[
        dcc.Graph(
            id='graph',
            figure=fig,
            config=config
        )])

app.layout = layout

if __name__ == '__main__':
    app.run_server(debug=True, use_reloader=True)
